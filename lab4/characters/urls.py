from django.urls import path
from . import views
  
urlpatterns = [
    path('create/', views.create_character, name='create'),
    path('getall/', views.get_characters, name='get'),
    path('get/<str:name>/', views.get_characters_id, name='get-id'),
    path('update/<str:name>/', views.update_character, name='update-id'),
    path('delete/<str:name>/', views.delete_character, name='delete-id'),
]