from django.db import models


class Characters(models.Model):
    name = models.CharField(unique=True, max_length=100)
    type = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    def __str__(self) -> str:
        return self.name
