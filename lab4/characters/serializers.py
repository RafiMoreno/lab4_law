from rest_framework import serializers
from .models import Characters
from django.db.models import fields

class CreateCharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Characters
        fields = '__all__'

class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Characters
        fields = ('name', 'type', 'description')