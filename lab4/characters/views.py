from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import serializers
from rest_framework import status
from .models import Characters
from .serializers import CharacterSerializer, CreateCharacterSerializer
from django.http import Http404

@api_view(['POST'])
def create_character(request):
    chara = CreateCharacterSerializer(data=request.data)
    if Characters.objects.filter(**request.data).exists():
        raise serializers.ValidationError('This data already exists')
  
    if chara.is_valid():
        chara.save()
        return Response(chara.data, status=status.HTTP_201_CREATED)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_characters(request):  
    chara = Characters.objects.all()
    serializer = CharacterSerializer(chara, many=True)
    if serializer:
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_characters_id(request, name):  
    try:
        chara = Characters.objects.get(name=name)
    except Characters.DoesNotExist:
        raise Http404("No Characters matches the given query.")
    serializer = CharacterSerializer(chara, many=False)
    if serializer:
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def update_character(request, name):
    try:
        chara = Characters.objects.get(name=name)
    except Characters.DoesNotExist:
        raise Http404("No Characters matches the given query.")
    serializer = CreateCharacterSerializer(instance=chara, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def delete_character(request, name):
    try:
        chara = Characters.objects.get(name=name)
    except Characters.DoesNotExist:
        raise Http404("No Characters matches the given query.")
    
    if chara:
        chara.delete()
        response = {
                    "message": "Item Deleted"
               }
        return Response(response, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)